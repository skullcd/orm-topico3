#singleton
import mysql.connector

class Connect(object):
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def conectar(self):
        self.conexion=mysql.connector.connect(  host="127.0.0.1",
          user="root",
          passwd="",
          database="topico3_1")

    def desconexion(self):
        self.conexion.close()

    def execute(self, consulta):
        self.cursor=self.conexion.cursor()
        registros = self.cursor.execute(consulta)
        try:
            registros=self.cursor.fetchall()
        except Exception as e:
            print(e)
        count = self.cursor.rowcount
        return  registros, count

    def modify(self, consulta):
        self.cursor=self.conexion.cursor()
        self.cursor.execute(consulta)

    def commit(self):
        self.conexion.commit()




# db = DB()
# db.conectar()
# db_1=DB()
# print(db )
# print(db)
# print(db.execute("SHOW DATABASES"))
