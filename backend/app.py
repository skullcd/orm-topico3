
from flask import Flask, request, Response
from Modules import ConcreteFactory
import json
import hashlib
router = ConcreteFactory()
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')
def index():
  return 'Server Works!'

@app.route('/login', methods=['POST'])
def _login():
  if request.method == 'POST':
    email = request.json["email"]
    password = request.json["password"]
    password = hashlib.sha256(password.encode('utf-8'))
    password = password.hexdigest()
    user =  router.getCRUD().getModel("user")
    json_response=json.dumps(user.login(email, password))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response


@app.route('/register', methods=['POST'])
def _register():
  if request.method == 'POST':

    email = request.json["email"]
    password = request.json["password"]
    first_name = request.json["first_name"]
    last_name = request.json["last_name"]
    type=request.json["type"]

    password = hashlib.sha256(password.encode('utf-8'))
    password = password.hexdigest()
    data={"nombre":first_name, "apellidos":last_name, "email":email, "password":password, "tipo":type}
    print(data)
    user =  router.getCRUD().getModel("user")
    json_response=json.dumps(user.register(data))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response


@app.route('/getModules/<user>')
def _getModules(user):
  data =  router.getCRUD().getModel("user")
  json_response=json.dumps(data.getModules(user))
  response=Response(json_response,content_type='application/json; charset=utf-8')
  response.headers.add('content-length',len(json_response))
  response.status_code=200
  return response


@app.route('/updatePermissionss', methods=['POST'])
def _updatePermissionss():
  if request.method == 'POST':
    user_id = request.json["user_id"]
    catalog_id = request.json["catalog_id"]
    permission_id = request.json["permissions_id"]

    user =  router.getCRUD().getModel("user")
    json_response=json.dumps(user.updatePermissions_aux(user_id, catalog_id, permission_id))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response



@app.route('/getPermissions/<user>')
def _getPermissions(user):
  catalog =  router.getCRUD().getModel("catalog")
  json_response=json.dumps(catalog.getPermissions(user))
  response=Response(json_response,content_type='application/json; charset=utf-8')
  response.headers.add('content-length',len(json_response))
  # Content-Disposition: attachment; filename="filename.jpg"
  response.status_code=200
  return response


@app.route('/getDataCatalog', methods=['POST'])
def _getDataCatalog():
  if request.method == 'POST':
    catalog_name = request.json["catalog_name"]
    print(catalog_name)
    catalog =  router.getCRUD().getModel("catalog")
    json_response=json.dumps(catalog.getDataCatalog(catalog_name))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response

@app.route('/createCatalog', methods=['POST'])
def _createCatalog():
  if request.method == 'POST':
    catalog_name = request.json["catalog_name"]
    catalog_data = request.json["catalog_data"]

    catalog =  router.getCRUD().getModel("catalog")
    json_response=json.dumps(catalog.createCatalog(catalog_name, catalog_data))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response



@app.route('/dropCatalog', methods=['POST'])
def _dropCatalog():
  if request.method == 'POST':
    catalog_name = request.json["catalog_name"]
    catalog_id = request.json["catalog_id"]
    user = request.json["user_id"]

    catalog =  router.getCRUD().getModel("catalog")
    json_response=json.dumps(catalog.dropCatalog(catalog_id,catalog_name, user))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response

@app.route('/insertCatalog', methods=['POST'])
def _insertCatalog():
  if request.method == 'POST':
    table = request.json["catalog_name"]
    catalog_data = request.json["catalog_data"]
    print(catalog_data)
    catalog =  router.getCRUD().getModel("catalog")
    json_response=json.dumps(catalog.insert(catalog_data, table))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response

@app.route('/updateCatalog', methods=['POST'])
def _updateCatalog():
  if request.method == 'POST':
    table = request.json["catalog_name"]
    catalog_data = request.json["catalog_data"]
    catalog_id = request.json["column_id"]

    catalog =  router.getCRUD().getModel("catalog")
    json_response=json.dumps(catalog.update(catalog_data, table, "id="+str(catalog_id)))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response


@app.route('/deleteCatalog', methods=['POST'])
def _deleteCatalog():
  if request.method == 'POST':
    table = request.json["catalog_name"]
    catalog_id = request.json["column_id"]

    catalog =  router.getReportes().getModel("catalog")
    json_response=json.dumps(catalog.delete(catalog_data, table, "id="+str(catalog_id)))
    response=Response(json_response,content_type='application/json; charset=utf-8')
    response.headers.add('content-length',len(json_response))
    response.status_code=200
    return response



@app.route('/getCsv', methods=['POST', "GET"])
def _getCsv():
    report =  router.getCSV().getDCSV("1")
    response=Response(report,content_type='text/csv; charset=utf-8')
    response.headers.add('Content-Disposition','attachmment; filename=prueba.csv')
    response.status_code=200
    print(response)
    return response



@app.route('/logs', methods=['POST', "GET"])
def _getReport():
  report =  router.getReportes().getReport("2009/05/6", "2029/6/12")
  response=Response(report,content_type='application/pdf; charset=utf-8')
  response.headers.add('Content-Disposition','attachmment; filename=test.pdf')
  response.status_code=200
  return response

# if __name__ == '__main__':
#     app.debug = True
#     app.run(host = '148.220.209.73',port=5000)
