from abc import ABC
from Models import ConcreteFactory as ConcreteFactory_Models
from PDF import Pdf
from Csv import CreateCSV

class AbstractFactory(ABC):
   def getCRUD(self): pass
   def getCSV(self): pass
   def getReportes(self): pass
   def getModel(self, *args): pass

class ConcreteFactory(AbstractFactory):
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def getCRUD(self):
       return CRUD()
    def getCSV(self):
        return CSV()
    def getReportes(self):
        return Reporte()

class CRUD:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
    def __init__(self):
        self.ConcreteFactory_Models = ConcreteFactory_Models()

    def getModel(self, name):
        instance=None
        def __new__(cls, *args, **kargs):
            if cls.instance is None:
                cls.instance=object.__new__(cls, *args, **kargs)
            return cls.instance
        model = ""
        if name == "user":
            return  self.ConcreteFactory_Models.getUsers()
        else:
            return  self.ConcreteFactory_Models.getCatalogos()

class CSV:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
    def __init__(self):
        self.__csv = CreateCSV()
    def getModel(self, nombre):
         return nombre
    def getDCSV(self, id):
        return self.__csv.csvGenerate(1)

class Reporte:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
        
    def __init__(self):
        self._pdf = Pdf()
    def getModel(self, nombre):
         return nombre
    def getReport(self, date1, date2):
        return self._pdf.pdfGenerate(date1,date2)



# factory = ConcreteFactory()
# crud = factory.getCRUD().getModel("user")

# print(crud.login("carlosd_eg@yahoo.com.mx", "Contrasenia"))
#
# print(crud.createUser("carlos"))
