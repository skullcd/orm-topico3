from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Frame, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4, landscape, portrait, letter
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from reportlab.pdfgen import canvas
from reportlab.platypus.doctemplate import PageTemplate
from datetime import datetime
import csv
from DB import DataBase

from http.client import HTTPResponse
from io import BytesIO

class Pdf:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
    def __init__(self):
        self.__db = DataBase()


    def pdfGenerate(self, date1, date2):
        logs = self.__db.select("id, accion, descripción, fecha, usuario_id", "logs", "fecha "+"BETWEEN CAST('"+date1+"' AS DATE) AND CAST('"+date2+"' AS DATE)")
        buffer = BytesIO()

        print(logs)

        #Datos
        datos1 = logs
        #Dar formato a lod datos de la DB
        #keys para el Diccionario
        element = ['#', 'action', 'description', 'datatime', 'user']

        datosBruto = []

        for d in datos1:
            datosBruto.append(d)

        daosBruto = datosBruto[:len(datosBruto) - 1]
        datosBruto = datosBruto[0][:len(datosBruto[0]) - 1]

        #Info de la db con formato
        reportes =[]

        for e in datosBruto:
            dic1 = {}
            dic1[element[0]] = e[0]
            dic1[element[1]] = e[1]
            dic1[element[2]] = e[2]
            dic1[element[3]] = e[3]
            dic1[element[4]] = e[4]
            reportes.append(dic1)


        pdfReportPages = "test.pdf" #Opcion LOCAL
        doc = SimpleDocTemplate(buffer, leftMargin= 1*cm, topMargin=2.5*cm, bottomMargin=1.5*cm, pagesize=landscape(letter))

        # container for the "Flowable" objects
        elements = []
        styles=getSampleStyleSheet()
        styleN = styles["Normal"]


        # Make heading for each column and start data list
        # Table header
        styles = getSampleStyleSheet()
        styleBH = styles['Heading1']
        # styleBH.alignment = TA_CENTER
        styleBH.fontSize = 10
        # Header de Tabla
        hnumero = Paragraph('''#''', styleBH)
        haction = Paragraph('''Acción''', styleBH)
        hdescription = Paragraph('''Descripción''', styleBH)
        hdate = Paragraph('''Fecha y Hora''', styleBH)
        huser = Paragraph('''Usuario''', styleBH)

        # Assemble data for each column using simple loop to append it into data list
        data = [[hnumero, haction, hdescription, hdate, huser]]
        high = 490  # donde iniciar la tabla depende de las columnas

        def myFirstPage(canvas, doc): #inner function
        # ancho de linea
            canvas.setLineWidth(.6)
            canvas.setStrokeColorRGB(176/255,196/255,222/255)
            canvas.setFillColorRGB(176/255,196/255,222/255)
            canvas.roundRect(0.5*cm,550,762,1.3*cm, 10, fill = 1)
            canvas.setPageSize(landscape(letter)) #Hacer horizontal la pag.
            # fuente y tamaño
            canvas.setFont('Courier-Bold', 22)
            # c.setFont('Helvetica', 14)
            # dibuja textoç
            canvas.setFillColorRGB(0,0,0)
            canvas.drawCentredString(400, 565, "Reporte del Sistema") #Donde escribe titulo(x,y)

            #agregar fecha y hora
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%d-%b-%Y   Hora: %H:%M:%S")
            canvas.setFont('Helvetica', 8)
            canvas.setFillColorRGB(0,0,0)
            canvas.drawString(5, 5, timestampStr) #Donde escribe fecha(x,y)


        contador = 0
        for dato in reportes:
            #print(reportes[0]['#'])
            reporte = [reportes[contador]['#'], reportes[contador]['action'], reportes[contador]['description'],
            reportes[contador]['datatime'], reportes[contador]['user']]
            data.append(reporte)
            contador = contador + 1
            high = high - 18

        tableThatSplitsOverPages = Table(data, colWidths=[0.9 * cm, 1.8 * cm, 15 * cm, 5 * cm, 3 * cm], repeatRows=1)
        tableThatSplitsOverPages.hAlign = 'LEFT'
        tblStyle = (TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black), ]))
        tblStyle.add('BACKGROUND',(0,0),(5,0),colors.lightblue)
        tblStyle.add('BACKGROUND',(0,1),(-1,-1),colors.white)
        tableThatSplitsOverPages.setStyle(tblStyle)
        elements.append(tableThatSplitsOverPages)

        doc.build(elements, onFirstPage=myFirstPage)

        pdf = buffer.getvalue()
        buffer.close()
        # response.write(pdf)
        return pdf
        #return 'PDF creado¡'

#
# pdf = Pdf()
# print(pdf.pdfGenerate("1"))
