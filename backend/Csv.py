import csv
from DB import DataBase


class CreateCSV:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def __init__(self):
        self.__db = DataBase()

    def csvGenerate(self, attr, table,where):
        datos = self.__db.select(attr, table)

        collection = []
        print(datos[0])
        for i in datos[0]:
            collection.append(list(i));

        with open('prueba.csv', 'w') as writeFile:
            writer = csv.writer(writeFile)
            writer.writerows(collection)
            writeFile.close()
        return "writeFile"


CreateCSV = CreateCSV()
CreateCSV.csvGenerate("*", "usuarios","")
