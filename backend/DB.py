from Connection import Connect
from QueryBuilder import Builder

class DataBase:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def __init__(self):
        self.__db = Connect()
        self.__db.conectar()
        self.__ORM = Builder()

    def select(self, attr, table, where = '', orderBy=''):
        statement = self.__ORM.select(attr, table, where, orderBy)
        result = self.__db.execute(statement)
        return result

    def insert(self, data, table):
        statement = self.__ORM.insert(data, table)
        result = self.__db.execute(statement)
        self.__db.commit()
        return result

    def update(self, data, table, where):
        statement = self.__ORM.update(data, table, where)
        result = self.__db.execute(statement)
        return result

    def delete(self, table, where):
         statement = self.__ORM.delete(table, where)
         result = self.__db.execute(statement)
         self.__db.commit()
         return result

    def create(self, name, data):
        statement = self.__ORM.create(name, data)
        result = self.__db.execute(statement)
        return result

    def drop(self, name):
        statement = self.__ORM.delete(name)
        result = self.__db.execute(statement)
        self.__db.commit()
        return result
