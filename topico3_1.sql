-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 04:24 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `topico3_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesos_menu`
--

CREATE TABLE `accesos_menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `modulos_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `accesos_menu`
--

INSERT INTO `accesos_menu` (`id`, `nombre`, `usuario_id`, `modulos_id`) VALUES
(1, 'Catalogo', 1, 1),
(2, 'Usuarios', 1, 2),
(3, 'Reportes', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `asignacion_permisos`
--

CREATE TABLE `asignacion_permisos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `permiso_id` int(11) NOT NULL,
  `catalogo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `asignacion_permisos`
--

INSERT INTO `asignacion_permisos` (`id`, `usuario_id`, `permiso_id`, `catalogo_id`) VALUES
(3, 1, 3, 8),
(4, 1, 4, 8),
(8, 1, 1, 2),
(13, 1, 1, 8),
(14, 1, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `catalogo`
--

CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `catalogo`
--

INSERT INTO `catalogo` (`id`, `nombre`, `estatus`) VALUES
(1, 'nueva_tabla', '1'),
(2, 'modulos', '1'),
(3, 'Productos', '1'),
(6, 'nueva_tabla', '1'),
(7, 'segunda_tabla', '1'),
(8, 'tercera_tabla', '1');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) DEFAULT NULL,
  `descripción` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `accion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `fecha`, `usuario_id`, `descripción`, `accion`) VALUES
(1, '2019-05-07 03:06:14', 1, 'Se elimino el permiso', 'delete'),
(2, '2019-06-07 03:06:14', 1, 'Se actualizo el permiso', 'update'),
(3, '2019-07-07 03:06:14', 1, 'Se inserto el permiso', 'create'),
(4, '2019-10-07 03:06:14', 1, 'Se elimino el permiso', 'delete'),
(5, '2019-10-07 08:14:25', 1, 'Se inseto el permiso', 'Insert'),
(6, '2019-10-07 08:14:25', 1, 'Se inseto el permiso', 'Insert'),
(7, '2019-10-07 08:14:54', 1, 'Se elimino el permiso', 'Delete'),
(8, '2019-10-07 08:14:54', 1, 'Se elimino el permiso', 'Delete'),
(9, '2019-10-07 08:30:33', 1, 'Se inserto el permiso', 'Insert'),
(10, '2019-10-07 08:30:33', 1, 'Se inserto el permiso', 'Insert');

-- --------------------------------------------------------

--
-- Table structure for table `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`, `icon`, `path`) VALUES
(1, 'Reportes', 'menu_book', '/reports'),
(2, 'Catalogos', 'view_quilt', '/catalogs'),
(3, 'Usuarios', 'face', '/users');

-- --------------------------------------------------------

--
-- Table structure for table `nueva_tabla`
--

CREATE TABLE `nueva_tabla` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `nueva_tabla`
--

INSERT INTO `nueva_tabla` (`id`, `nombre`) VALUES
(NULL, 'jhjh'),
(NULL, 'jhj'),
(NULL, 'jkjk'),
(NULL, 'jkjk'),
(NULL, 'hola'),
(NULL, 'holll'),
(NULL, 'adios'),
(NULL, 'bue');

-- --------------------------------------------------------

--
-- Table structure for table `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `permisos`
--

INSERT INTO `permisos` (`id`, `nombre`, `descripcion`, `estatus`) VALUES
(1, 'Read', 'Puede leer el archivo', '1'),
(2, 'update', 'puede actualizar', '1'),
(3, 'create ', 'puede crear', '1'),
(4, 'dalete', 'puede eliminar', '1');

-- --------------------------------------------------------

--
-- Table structure for table `segunda_tabla`
--

CREATE TABLE `segunda_tabla` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `segunda_tabla`
--

INSERT INTO `segunda_tabla` (`id`, `nombre`) VALUES
(NULL, 'gg');

-- --------------------------------------------------------

--
-- Table structure for table `sisirve`
--

CREATE TABLE `sisirve` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tercera_tabla`
--

CREATE TABLE `tercera_tabla` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `tercera_tabla`
--

INSERT INTO `tercera_tabla` (`id`, `nombre`) VALUES
(1, 'hola');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `tipo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `password`, `activo`, `tipo`) VALUES
(1, 'Carlos', 'Estrada', 'carlosd_Eg@yahoo.com.mx', '33dc0ba86008f4434bd43d050df9022209367483c1eef5280b25da861c32f6ad', 1, 'Admin'),
(2, 'Daniel', 'Estrada', 'carlosd_edg@yahoo.com.mx', '33dc0ba86008f4434bd43d050df9022209367483c1eef5280b25da861c32f6ad', 0, 'User'),
(6, 'Sharol', 'Gonzalez', 'sharol@gmail.com', '61503690505f84b144e6ac89124540a3eb8d22e77db76500984cfc50a1d8776e', 0, 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesos_menu`
--
ALTER TABLE `accesos_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`),
  ADD KEY `modulos_id` (`modulos_id`);

--
-- Indexes for table `asignacion_permisos`
--
ALTER TABLE `asignacion_permisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`),
  ADD KEY `permiso_id` (`permiso_id`),
  ADD KEY `catalogo_id` (`catalogo_id`);

--
-- Indexes for table `catalogo`
--
ALTER TABLE `catalogo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indexes for table `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tercera_tabla`
--
ALTER TABLE `tercera_tabla`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accesos_menu`
--
ALTER TABLE `accesos_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `asignacion_permisos`
--
ALTER TABLE `asignacion_permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `catalogo`
--
ALTER TABLE `catalogo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tercera_tabla`
--
ALTER TABLE `tercera_tabla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accesos_menu`
--
ALTER TABLE `accesos_menu`
  ADD CONSTRAINT `accesos_menu_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `accesos_menu_ibfk_2` FOREIGN KEY (`modulos_id`) REFERENCES `catalogo` (`id`),
  ADD CONSTRAINT `accesos_menu_ibfk_3` FOREIGN KEY (`modulos_id`) REFERENCES `modulos` (`id`),
  ADD CONSTRAINT `accesos_menu_ibfk_4` FOREIGN KEY (`modulos_id`) REFERENCES `modulos` (`id`);

--
-- Constraints for table `asignacion_permisos`
--
ALTER TABLE `asignacion_permisos`
  ADD CONSTRAINT `asignacion_permisos_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `asignacion_permisos_ibfk_2` FOREIGN KEY (`permiso_id`) REFERENCES `permisos` (`id`),
  ADD CONSTRAINT `asignacion_permisos_ibfk_3` FOREIGN KEY (`catalogo_id`) REFERENCES `catalogo` (`id`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
